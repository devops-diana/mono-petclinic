pipeline {
    agent none
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                sh 'mvn clean package -B -ntp'
            }
        }
        stage('Testing') {
            agent any
            steps {
                jacoco()
            }
        }
        stage('Sonar') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                withCredentials([file(credentialsId: 'settings-sonar', variable: 'CONFIG_SETTINGS')]) {
                    withSonarQubeEnv(installationName: 'SonarAnalysis') {
                        sh "mvn sonar:sonar -B -ntp -s ${CONFIG_SETTINGS}"
                    }
                }
            }
        }
        stage('QualityGate') {
            agent any
            steps {
                timeout(time: 4, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Artifactory') {
            agent any
            steps {
                script {
                    def pom = readMavenPom file: 'pom.xml'
                    def pomVersion = pom.version
                    def server = Artifactory.server 'artifactory-server'
                    def repository = 'spring-petclinic-mono-'

                    if(pomVersion.toLowerCase().contains('release')){
                        repository = repository + 'release'
                    } else {
                        repository = repository + 'snapshot'
                    }

                    def uploadSpec = """
                        {
                            "files": [
                                {
                                    "pattern": "target/.*.jar",
                                    "target": "${repository}",
                                    "regexp": "true"
                                }
                            ]
                        }
                    """
                    server.upload spec: uploadSpec
                }
            }
        }
        stage('Deployment') {
            agent {
                docker {
                    image 'ansible/ansible-runner:1.4.7'
                    args '-u root'
                }
            }
            environment {
                ANSIBLE_HOST_KEY_CHECKING = "False"
            }
            steps {
                sh 'ansible --version'
                sshagent (credentials: ['deploy-server-key']) {
                    sh 'ansible-inventory -i hosts --list'

                    sh 'ansible-playbook -i hosts deployArtifact.yml'
                }
            }
        }
    }
}
