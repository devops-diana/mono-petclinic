# Proyecto Final Curso Mitocode - Arquitecto Devops
Tipo de Arquitectura: Monolítica

Nota: Tuve problemas para desplegar el artefacto como tipo war, por lo que al final lo cambie a jar para que pudiera funcionar el despliegue en JBoss

Se presenta una pipeline de Jenkins que pasa por los siguientes stages:

## Build

```bash
       stage('Build') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                sh 'mvn clean package -B -ntp'
            }
        }
```
## Testing

```bash
       stage('Testing') {
            agent any
            steps {
                jacoco()
            }
        }
```
## Sonar

```bash
       stage('Sonar') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                }
            }
            steps {
                withCredentials([file(credentialsId: 'settings-sonar', variable: 'CONFIG_SETTINGS')]) {
                    withSonarQubeEnv(installationName: 'SonarAnalysis') {
                        sh "mvn sonar:sonar -B -ntp -s ${CONFIG_SETTINGS}"
                    }
                }
            }
        }
```
## QualityGate

```bash
       stage('QualityGate') {
            agent any
            steps {
                timeout(time: 4, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
```
## Artifactory

```bash
       stage('Artifactory') {
            agent any
            steps {
                script {
                    def pom = readMavenPom file: 'pom.xml'
                    def pomVersion = pom.version
                    def server = Artifactory.server 'artifactory-server'
                    def repository = 'spring-petclinic-mono-'

                    if(pomVersion.toLowerCase().contains('release')){
                        repository = repository + 'release'
                    } else {
                        repository = repository + 'snapshot'
                    }

                    def uploadSpec = """
                        {
                            "files": [
                                {
                                    "pattern": "target/.*.jar",
                                    "target": "${repository}",
                                    "regexp": "true"
                                }
                            ]
                        }
                    """
                    server.upload spec: uploadSpec
                }
            }
        }
```
## Deployment

```bash
       stage('Deployment') {
            agent {
                docker {
                    image 'ansible/ansible-runner:1.4.7'
                    args '-u root'
                }
            }
            environment {
                ANSIBLE_HOST_KEY_CHECKING = "False"
            }
            steps {
                sh 'ansible --version'
                sshagent (credentials: ['deploy-server-key']) {
                    sh 'ansible-inventory -i hosts --list'

                    sh 'ansible-playbook -i hosts deployArtifact.yml'
                }
            }
        }
```
## Archivos Para Despliegue (Ansible)

### deployArtifact.yml
```bash
---
- hosts: serverCentos
  become: true
  remote_user: root
  tasks:
  - name: Copy war
    ansible.builtin.copy:
      src: target/spring-petclinic-mono-2.6.0-SNAPSHOT.jar
      dest: /home

  - name: Undeploy war
    community.general.jboss:
      deploy_path: /home/jboss-eap-7.4/standalone/deployments
      deployment: spring-petclinic-mono-2.6.0-SNAPSHOT.jar
      state: absent

  - name: Deploy war
    community.general.jboss:
      src: /home/spring-petclinic-mono-2.6.0-SNAPSHOT.jar
      deploy_path: /home/jboss-eap-7.4/standalone/deployments
      deployment: spring-petclinic-mono-2.6.0-SNAPSHOT.jar
      state: present

  - name: Remove war
    ansible.builtin.file:
      path: /home/spring-petclinic-mono-2.6.0-SNAPSHOT.jar
      state: absent
```

### hosts
```
[servers]
serverCentos ansible_host=104.200.17.167
```
